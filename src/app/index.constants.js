/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('wallaRssFeeder')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
