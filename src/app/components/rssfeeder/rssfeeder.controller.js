'use strict';
google.load("feeds", "1");

angular.module('wallaRssFeeder')

  .service('rssFeed', function($q, $rootScope) {
    this.get = function(url) {
      var d = $q.defer();
      var feed = new google.feeds.Feed(url);
      feed.setNumEntries(50);
      feed.load(function(result) {
        $rootScope.$apply(d.resolve(result));
      });
      return d.promise;
    }
  })
  .controller('rssFeederCtrl', function ($scope,$http,rssFeed) {

    //  selectedObj - I used this scope object to send the url in this controller also
    //  ( I can do this because they have the same scope )

    $scope.rssfeedsObj = {};
    $scope.rssFeedArr = [];
    $scope.initRssFeedArray = function(){

      if ($scope.selectedObj) {
       /* var rssTofetch = "http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&q=" + $scope.selectedObj.url;
          // this implementation cased getting from server access control cross domain error,
          it took me a while to find the correct way, but I did it :-)

				$http.get(rssTofetch).success(function (data, status, headers, config) {
				  if (data.responseData) {
					$scope.rssfeedsObj = data.responseData.feed.entries;
					$scope.rssFeedUrl = data.responseData.feed.feedUrl;
				  }
				}).error(function (data, status, headers, config) {
				  alert("An error occurred during the AJAX request");
				});

        */

        rssFeed.get($scope.selectedObj.url).then(function(result) {
          if (result.error) {
            alert("ERROR " + result.error.code + ": " + result.error.message + "\nurl: " + url);
            $scope.setLoading(false);
          }
          else {
            $scope.rssfeedsObj = result.feed.entries;
            $scope.rssFeedUrl = result.feed.feedUrl;
          }
        });

/*

        var req = {
          method: 'GET',
          url: rssTofetch,
          headers: {
            'Access-Control-Allow-Origin': '*'
          }
        }
*/
       // $http(req).then(function(){console.log("good");}, function(){console.log("bad");});

      }
    };

    $scope.getFeedUrl = function(){

      return $scope.rssFeedUrl;
    };

    $scope.$watch('selectedObj', function () {
      $scope.initRssFeedArray();
    })

  })


  .directive('rssFeeder', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/components/rssfeeder/rssfeeder.html',
      controller: 'rssFeederCtrl',
    }
  });
