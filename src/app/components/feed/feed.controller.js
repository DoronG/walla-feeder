'use strict';

angular.module('wallaRssFeeder')
  .controller('FeedCtrl', function ($scope) {

    $scope.getFeedHeader = function (){

      return $scope.feedinfo.title;
    }

    $scope.getPublishedDate = function (){

      return $scope.feedinfo.publishedDate;
    }

    $scope.getContent = function (){
      
      return $scope.feedinfo.content;
    }

  })
  .directive('feed', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/components/feed/feed.html',
      controller: 'FeedCtrl',
      scope: {
        feedinfo: '=?'

      }
    }
  });
