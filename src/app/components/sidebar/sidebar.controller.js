'use strict';


angular.module('wallaRssFeeder')
  .controller('SideBarCtrl', function ($scope,$location) {

    $scope.urlList=[];
    $scope.selectedUrlindex;
    $scope.selectedObj={};

    $scope.addUrlToList = function (i_url) {

      if (i_url) {

        var isUrlFound = $scope.urlList.filter(isUrlExsis,i_url);

        if (isUrlFound.length > 0)
        {
            alert(i_url + " already exists in history!");
        }
        else {

          $scope.urlList.push({url: i_url, isSelected: true});
          $scope.setSelectedObject($scope.urlList.length - 1);
          // I want to push the previous url (if it exists), if not push the first url
          var urlTopush = $scope.urlList.length > 0 ? $scope.urlList[$scope.selectedUrlindex].url : i_url;
          history.pushState({url: urlTopush}, "back to same location", "aaa");
          console.log("addUrlToList pushState: " + i_url);

          $scope.selectedUrlindex = $scope.urlList.length - 1;
          console.log(i_url + " added.");

          var innerUrl = $location.path();
          $location.path(innerUrl);

        }
      }
    };

    function isUrlExsis(i_urlObj) {

      return i_urlObj.url === this;
    };



    $scope.setSelectedObject = function(index){
      if (index >= 0) {
        if ($scope.selectedUrlindex >= 0 && $scope.urlList.length > 1 ) {
          $scope.urlList[$scope.selectedUrlindex].isSelected = false;
        }
        $scope.selectedObj = $scope.urlList[index];
      }

    };

    $scope.VerifySelectedUrl = function(index){

      return $scope.urlList[index].isSelected;
    }

    $scope.selectUrl = function(index){
      history.pushState({url: $scope.urlList[$scope.selectedUrlindex].url}, "back to same location", "url");
      $scope.urlList[$scope.selectedUrlindex].isSelected = false;
      $scope.selectedUrlindex = index;
      $scope.urlList[$scope.selectedUrlindex].isSelected = true;
      $scope.selectedObj = $scope.urlList[$scope.selectedUrlindex];

      var innerUrl = $location.path();
      $location.path(innerUrl);

    };

    $scope.removeUrl = function (index) {

      if ($scope.selectedObj.url === $scope.urlList[index].url){
      //  After deleting feed we will see it after page refresh (wont be saved inside local data).
        $scope.selectedObj.url = {};
      }

      $scope.urlList.splice(index,1);
      console.log("removed url"+index);
      $scope.selectedUrlindex = $scope.urlList.length > 0 ? 0 : undefined ;
    };

    function afterReload()
    {
      var wallaUrlsInfo = JSON.parse(localStorage.getItem("wallaUrlsInfo"));

      console.log("after reloaded");
      if (wallaUrlsInfo) {
        $scope.urlList = wallaUrlsInfo.urlsObj || [];
        $scope.selectedObj = wallaUrlsInfo.selectedUrlObj || {};
        $scope.selectedUrlindex = wallaUrlsInfo.selectedUrlindex || 0;

      }
    };

    $scope.getIndexOfUrl = function(i_url){

      var index;
      for (index = 0 ; index < $scope.urlList.length; index++)
      {
        if ($scope.urlList[index].url === i_url){
          break;
        }
      }

      return index;
    };

    function beforeReload()
    {
      var wallaUrlsInfo = {

        urlsObj : $scope.urlList,
        selectedUrlObj : $scope.selectedObj,
        selectedUrlindex : $scope.selectedUrlindex
    };

      localStorage.setItem("wallaUrlsInfo", JSON.stringify(wallaUrlsInfo));
      console.log("before reloaded");
    }

    window.addEventListener("beforeunload", function(event) {
      beforeReload();
    });

    $(document).ready(function () {
      afterReload();
    });

    window.onpopstate = function(event) {
      if (event.state) {
        console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
        $scope.selectUrl($scope.getIndexOfUrl(event.state.url));
      }
    };

  })
  .directive('sideBar', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/components/sidebar/sidebar.html',
      controller: 'SideBarCtrl'
    }
  });
