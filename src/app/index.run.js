(function() {
  'use strict';

  angular
    .module('wallaRssFeeder')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
